<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Firebase\JWT\JWT;

class Users extends Controller {

    public function signup(Request $request) {
        if ($request->isJson()) {
            $secretkey = "totitoldi";
            $data = $request->json()->all();
            if (!(DB::select("SELECT * FROM Users WHERE emailUser = ?", [$data['email']]))) {
                try {
                    $values = [
                        $data['email'],
                        Hash::make($data['password']),
                        $data['profilePicture']
                    ];
                    DB::insert("INSERT INTO Users SET emailUser = ?, `password` = ?, profilePicture = ?", $values);
                    $token = array(
                        "email" => $data['email'],
                        "iat" => time(),
                        "exp" => time() + 60 * 60 * 2 
                    );
                    $jwt = JWT::encode($token, $secretkey);                
                    return response()->json([
                        'message' => 'You have registered successfully',
                        'token' => $jwt
                    ], 201);
                } catch(\Exception $e) {
                    return response()->json(['error' => 'The data is not in the correct format'], 400);
                }
            } else
                return response()->json(['error' => 'An account with this email already exists'], 409);    
        } else
            return response()->json(['error' => 'The request is not allowed'], 400);
    }

    public function signin(Request $request) {
        if ($request->isJson()) {
            $secretkey = "totitoldi";
            $data = $request->json()->all();
            if (DB::select("SELECT * FROM Users WHERE emailUser = ?", [$data['email']])) {
                $passwordUser = DB::select("SELECT `password` FROM Users WHERE emailUser = ?", [$data['email']]);
                $hashedPassword = $passwordUser[0] -> password;
                if (Hash::check($data['password'], $hashedPassword)) {
                    $token = array(
                        "email" => $data['email'],
                        "iat" => time(),
                        "exp" => time() + 60 * 60 * 3 
                    );
                    DB::update('UPDATE users set lastlogin = now() WHERE emailUser = ?', [$data['email']]);
                    $jwt = JWT::encode($token, $secretkey);                
                    return response()->json([
                        'message' => 'You have successfully logged in',
                        'token' => $jwt
                    ], 201);
                } else
                    return response()->json(['error' => 'Password is incorrect'], 401);
            } else
                return response()->json(['error' => 'The indicated account does not exist'], 404);
        } else
            return response()->json(['error' => 'The request is not allowed'], 400);
    }
}
