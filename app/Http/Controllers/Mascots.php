<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;

class Mascots extends Controller {

    public function index(Request $request) {
        $results = DB::select("SELECT * FROM Pets");
        return response()->json($results, 200);     
    }

    public function addPet(Request $request) {
        if ($request->isJson()) {
            $data = $request->json()->all();
            try {
                $values = [
                    $data['name'],
                    $data['age'],
                    $data['race'],
                    $data['owner']
                ];
                DB::insert("INSERT INTO Pets SET namePet = ?, agePet = ?, racePet = ?, ownerPet = ?", $values);
                return response()->json(['message' => 'Mascot added successfully'], 201);
            } catch(\Exception $e) {
                return response()->json(['message' => 'The data is not in the correct format'], 400);
            }
        } else
            return response()->json(['message' => 'The request is not allowed'], 400);
    }

    public function updatePet(Request $request, $id) {
        if ($request->isJson()) {
            if (DB::select("SELECT * FROM Pets WHERE keyPet = ?", [$id])) {
                $data = $request->json()->all();
                try {
                    $values = [
                        $data['name'],
                        $data['age'],
                        $data['race'],
                        $data['owner'],
                        $id
                    ];
                    DB::update("UPDATE Pets SET namePet = ?, agePet = ?, racePet = ?, ownerPet = ? WHERE keyPet = ?", $values);
                    return response()->json(['message' => 'Mascot modified correctly'], 201);
                } catch(\Exception $e) {
                    return response()->json(['error' => 'The data is not in the correct format'], 400);
                }
            } else
                return response()->json(['error' => 'Resource not found'], 404);        
        } else
            return response()->json(['error' => 'The request is not allowed'], 400);
    }

    public function deletePet(Request $request, $id) {
        if (DB::select("SELECT * FROM Pets WHERE keyPet = ?", [$id])) {
            try {
                DB::delete("DELETE FROM Pets WHERE keyPet = ?", [$id]);
                return response()->json(['message' => 'Pet deleted correctly'], 200);
            } catch(\Exception $e) {
                return response()->json(['error' => 'The pet could not be eliminated'], 400);
            }
        } else
            return response()->json(['error' => 'Resource not found'], 404);
    }
}