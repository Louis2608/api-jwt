<?php

// $router->get('/mascots', 'Mascots@index');


$router->group(['prefix' => 'mascots', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/', 'Mascots@index');
    $router->post('/', 'Mascots@addPet');
    $router->put('/{id}', 'Mascots@updatePet');
    $router->delete('/{id}', 'Mascots@deletePet');
});

$router->post('/signup', 'users@signup');
$router->put('/signin', 'users@signin');